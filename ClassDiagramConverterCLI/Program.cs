﻿using System;

namespace ClassDiagramConverterCLI
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args[0] != null)
                Console.WriteLine($"Hello {args[0]}!");
        }
    }
}
